package com.kalharbi.android.timer.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.kalharbi.android.timer.R;
import com.kalharbi.android.timer.services.TimerService;
import com.kalharbi.android.timer.utils.Constants;


public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getName();

    private TextView textViewHours, textViewMinutes, textViewSeconds;
    private NumberPicker hoursNumberPicker, minutesNumberPicker, secondsNumberPicker;

    // Flag to indicate whether we have called bind on the service or not.
    private boolean mBound;

    // The Messenger is an interface for communicating with the service.
    private Messenger mMessenger;

    /**
     * Inner class for communicating with the main interface of the service.
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        /*
          This is called when the connection with the service has been
          established, giving us the object we can use to
          interact with the service. We are communicating with the
          service using a Messenger, so here we get a client-side
          representation of that from the raw IBinder object.
        */
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mMessenger = new Messenger(iBinder);
            mBound = true;
            Log.d(TAG, "Service had been connected");
        }

        /* This is called when the connection with the service has been
           unexpectedly disconnected -- that is, its process crashed.
        */
        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mMessenger = null;
            mBound = false;
            Log.d(TAG, "Service had been disconnected");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTimerService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimerService();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void initViews() {
        textViewHours = (TextView) findViewById(R.id.textViewHoursValue);
        textViewMinutes = (TextView) findViewById(R.id.textViewMinutesValue);
        textViewSeconds = (TextView) findViewById(R.id.textViewSecondsValue);
        hoursNumberPicker = (NumberPicker) findViewById(R.id.hNumberPicker);
        minutesNumberPicker = (NumberPicker) findViewById(R.id.mNumberPicker);
        secondsNumberPicker = (NumberPicker) findViewById(R.id.sNumberPicker);

        hoursNumberPicker.setMaxValue(12);
        hoursNumberPicker.setMinValue(0);
        minutesNumberPicker.setMaxValue(60);
        minutesNumberPicker.setMinValue(0);
        secondsNumberPicker.setMaxValue(60);
        secondsNumberPicker.setMinValue(0);
    }

    public void doClick(View view) {
        if (view.getId() == R.id.button_start) {
            startCounterService();
        } else if (view.getId() == R.id.button_reset) {
            stopTimerService();
            resetTimerViews();
        }
    }

    // Bind to the service
    private void startTimerService() {
        if (!mBound) {
            // Bind to the service and automatically create the service as long as the binding exists.
            bindService(new Intent(this, TimerService.class), serviceConnection,
                    Context.BIND_AUTO_CREATE);
            mBound = true;
        }
    }

    // Unbind from the service
    private void stopTimerService() {
        if (mBound) {
            // create and send a message to the service, using a supported 'what' value.
            Message requestMessage = Message.obtain(null, Constants.STOP_COUNTER, 0, 0);
            requestMessage.replyTo = new Messenger(new ServiceResponseHandler(this));
            try {
                mMessenger.send(requestMessage);
                Log.d(TAG, "The client has sent a message to the service.");
            } catch (RemoteException e) {
                Log.e(TAG, e.getMessage());
            }
            unbindService(serviceConnection);
            mBound = false;
        }
    }

    // Send message to start the task in the counter service
    private void startCounterService() {
        if (!mBound) {
            Log.d(TAG, "Rebinding to the service..");
            startTimerService();
        }
        // create and send a message to the service, using a supported 'what' value.
        Message requestMessage = Message.obtain(null, Constants.START_COUNTER, 0, 0);
        Bundle requestBundle = new Bundle();
        int timeInSeconds = (hoursNumberPicker.getValue() * 3600) + (minutesNumberPicker.getValue() * 60)
                + secondsNumberPicker.getValue();
        requestBundle.putInt(Constants.START_VALUE_KEY, timeInSeconds);
        requestMessage.setData(requestBundle);
        requestMessage.replyTo = new Messenger(new ServiceResponseHandler(this));
        try {
            mMessenger.send(requestMessage);
            Log.d(TAG, "The client has sent a message to the service.");
        } catch (RemoteException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    // Reset time text views.
    private void resetTimerViews() {
        textViewHours.setText("00");
        textViewMinutes.setText("00");
        textViewSeconds.setText("00");
    }

    // Show notification when the timer finishes.
    public void ShowNotification(String title, String message) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notify_status)
                        .setContentTitle(title)
                        .setContentText(message);
        // The notification is automatically canceled when the user clicks it.
        mBuilder.setAutoCancel(true);
        // Creates an explicit intent for the action to go directly from notification to the app
        Intent resultIntent = new Intent(this, MainActivity.class);
        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // this application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(MainActivity.this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId =2014 allows you to update the notification later on.
        mNotificationManager.notify(2014, mBuilder.build());
    }

    /**
     * Handle reply messages coming from the Bound Service (TimerService).
     */
    class ServiceResponseHandler extends Handler {
        private MainActivity mActivity;

        public ServiceResponseHandler(MainActivity activity) {
            this.mActivity = activity;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.STATUS_RESPONSE: {
                    String response = msg.getData().getString(Constants.STATUS_KEY);
                    if (response.equals("DONE")) {
                        resetTimerViews();
                        ShowNotification("CountDown Timer", "CountDown finished");
                    }
                }
                case Constants.PROGRESS_RESPONSE: {
                    long time = msg.getData().getLong(Constants.PROGRESS_KEY, 0);
                    long seconds = time % 60;
                    long minutes = time / 60;
                    long hours = time / 3600;
                    Log.d(TAG, "Received: " + Long.toString(time) + " seconds.");
                    textViewSeconds.setText(String.format("%02d", (seconds)));
                    textViewMinutes.setText(String.format("%02d", (minutes)));
                    textViewHours.setText(String.format("%02d", (hours)));
                }
            }
        }
    }

}