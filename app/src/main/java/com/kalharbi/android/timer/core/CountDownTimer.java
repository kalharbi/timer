package com.kalharbi.android.timer.core;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Khalid Alharbi.
 */
public class CountDownTimer {
    private final Timer timer;
    private AtomicBoolean isFinished;
    private long startCounterValueSeconds;

    public CountDownTimer(long startCounterValueSeconds) {
        this.startCounterValueSeconds = startCounterValueSeconds;
        this.isFinished = new AtomicBoolean(false);
        timer = new Timer();
    }

    public int start() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                startCounterValueSeconds--;
                if (startCounterValueSeconds < 0) {
                    timer.cancel();
                    isFinished.set(true);
                }
            }
        }, 0, 1000);
        return 0;
    }

    public void stop() {
        if (timer != null) {
            this.isFinished.set(true);
            timer.cancel();
        }
    }

    public boolean isFinished() {
        return this.isFinished.get();
    }

    public long getStartCounterValueSeconds() {
        return startCounterValueSeconds;
    }
}
